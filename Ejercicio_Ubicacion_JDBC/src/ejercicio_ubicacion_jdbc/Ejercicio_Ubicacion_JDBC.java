/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio_ubicacion_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jorge
 */
public class Ejercicio_Ubicacion_JDBC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException{
        
        Connection conn = null;
        Statement st = null;
        String url = "jdbc:mysql://localhost/bd_avanzada";
        String user = "root";
        String password = "";
        
        List<Alumno> alumnos = new ArrayList();
        
        
        try {
            
            conn = DriverManager.getConnection(url, user, password);
            st = conn.createStatement();
            
            StringBuilder consulta = new StringBuilder();
            consulta.append("SELECT * FROM alumnos");
            
            ResultSet rs = st.executeQuery(consulta.toString());
            
            while(rs.next()){
               Alumno a = new Alumno();
               
               a.setId(rs.getInt("id"));
               a.setNombre(rs.getString("nombre"));
               a.setEdad(rs.getInt("edad"));
               a.setCarrera(rs.getString("carrera"));
               a.setMaterias(rs.getInt("materias"));
                
               alumnos.add(a);
            }
            
            
            /*
                -- Obtener la edad promedio de todos los alumnos
                -- AVG()   AVERAGE determina un promedio
            
            */
            
            StringBuilder sqlProm = new StringBuilder();
            sqlProm.append("SELECT AVG(edad) AS 'promedio' ")
                   .append(" FROM alumnos ");
            
            ResultSet rsProm =  st.executeQuery(sqlProm.toString());
            int promedio = 0;
            if(rsProm.next()){
                promedio = rsProm.getInt("promedio");
            }
            
            System.out.println("El promedio de edades de los alumnos es "+promedio);
            
            
            /*
                -- Muestre el nombre del alumno con mayor edad y el de menor edad            
            */
            
            StringBuilder sqlMenorMayor = new StringBuilder();
            
            sqlMenorMayor.append("SELECT ")
                                .append(" nombre, edad ")
                         .append("FROM alumnos ")
                         .append("WHERE ")
                                .append(" edad = (SELECT MAX(edad) FROM alumnos LIMIT 1) ")
                                .append(" OR edad = (SELECT MIN(edad) FROM alumnos LIMIT 1) ");
            
            ResultSet rsMinMax = st.executeQuery(sqlMenorMayor.toString());
            System.out.println("====Alumnos Mayor y Menor Edad =====");
            while(rsMinMax.next()){
                System.out.println("El alumno "+rsMinMax.getString("nombre")+ " tiene "
                                   + rsMinMax.getInt("edad")+ " años.");
            }
            
            
            /*3.-Muestre el nombre de los alumnos que cuentan con el mayor número de materias.*/
            StringBuilder sqlMaxMaterias = new StringBuilder();
            sqlMaxMaterias.append("SELECT nombre FROM alumnos ")
                          .append(" WHERE materias = (SELECT MAX(materias) FROM alumnos LIMIT 1) ");
            
            
            ResultSet rsMaxM = st.executeQuery(sqlMaxMaterias.toString());
            System.out.println("========== ALUMNOS CON MAS MATERIAS =======");
            while(rsMaxM.next()){
                
                System.out.println(rsMaxM.getString("nombre"));
                
            }
            
            
            /*
            4.-Actualice el nombre de María y coloque “María del Rosario”
            */
            StringBuilder sqlMaria = new StringBuilder();
            String nuevoNombre = "María del Rosario";
            
            sqlMaria.append("UPDATE alumnos SET nombre = '")
                    .append(nuevoNombre).append("' ")
                    .append("WHERE nombre LIKE 'María' ");
            
            int fila = st.executeUpdate(sqlMaria.toString());
            if(fila > 0){
                System.out.println("María se actualizó correctamente. ");
            }else{
                System.out.println("No se pudo actualizar el registro. ");
            }
            
            /*
                5.-Elimine a Brian
            */
            StringBuilder sqlBrian = new StringBuilder();
            sqlBrian.append("DELETE FROM alumnos WHERE nombre LIKE 'Brian' ");
            
            int fila2 = st.executeUpdate(sqlBrian.toString());
            
            if(fila2 > 0){
                System.out.println("Brian se eliminó correctamente");
            }else{
                System.out.println("No se pudo eliminar a Brian");
            }
            
        } catch (Exception e) {
            System.out.println("Error: "+e.getMessage());
        }finally{
            //Cerramos Statement y Connection
            if(st != null){
                st.close();
            }
            
            if(conn != null){
                conn.close();
            }
            
        }
        
        
        //Imprimir lista de alumnos
        for(Alumno al : alumnos){
            System.out.println(al.toString());
        }
        
        
    }
    
}
