
package dao;

import configuracion.DBHelper;
import entidades.Alumno;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOAlumnoImpl implements DAOAlumno{

    private DBHelper db = DBHelper.getDB();
    
    @Override
    public int agregar(Alumno alumno) {
        int fila = -1;
        
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO alumnos (nombre,edad,carrera,materias)")
               .append(" VALUES (")
                    .append("'").append(alumno.getNombre()).append("',")
                    .append(alumno.getEdad()).append(",")
                    .append("'").append(alumno.getCarrera()).append("',")
                    .append(alumno.getMaterias())
                    .append(");");
            
            if(db.connect()){//Existe conexion
                fila = (int) db.execute(sql.toString(), true);
            }else{
                System.out.println("Error de conexion: "+db.getError());
            }
            
        } catch (Exception e) {            
            System.out.println(e.getMessage());
            
        }finally{
            db.disconnect();
        }
                
        return fila;
    }

    @Override
    public int editar(int id, Alumno alumno) {
       int fila = -1;
        
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE alumnos SET ")
                    .append(" nombre = '").append(alumno.getNombre()).append("',")
                    .append(" edad = ").append(alumno.getEdad()).append(",")
                    .append(" carrera = '").append(alumno.getCarrera()).append("',")
                    .append(" materias = ").append(alumno.getMaterias())
                    .append(" WHERE id = ").append(id);
            
            if(db.connect()){//Existe conexion
                fila = (int) db.execute(sql.toString(), true);
            }
            
        } catch (Exception e) {            
            System.out.println(e.getMessage());
            
        }finally{
            db.disconnect();
        }
                
        return fila;
    }

    @Override
    public int eliminar(int id) {
        int fila = -1;
        
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM alumnos ")
                    .append(" WHERE id = ").append(id);
            
            if(db.connect()){//Existe conexion
                fila = (int) db.execute(sql.toString(), true);
            }
            
        } catch (Exception e) {            
            System.out.println(e.getMessage());
            
        }finally{
            db.disconnect();
        }
                
        return fila;
    }

    @Override
    public List<Alumno> obtenerTodos() {
        List<Alumno> alumnos = new ArrayList();
        
        try {
            StringBuilder sql = new StringBuilder("SELECT * FROM alumnos ");
            
            if(db.connect()){
               ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
               
               while(rs.next()){
                   Alumno a = new Alumno();
                   
                   a.setId(rs.getInt("id"));
                   a.setNombre(rs.getString("nombre"));
                   a.setEdad(rs.getInt("edad"));
                   a.setCarrera(rs.getString("carrera"));
                   a.setMaterias(rs.getInt("materias"));
                   
                   alumnos.add(a);
               }
               
            }
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }finally{
            db.disconnect();
        }
        
        return alumnos;
    }

    @Override
    public List<Alumno> obtenerPorNombre(String nombre) {
         List<Alumno> alumnos = new ArrayList();
        
        try {
            StringBuilder sql = new StringBuilder("SELECT * FROM alumnos ");
            sql.append(" WHERE nombre LIKE '%").append(nombre).append("%' ");
            if(db.connect()){
               ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
               
               while(rs.next()){
                   Alumno a = new Alumno();
                   
                   a.setId(rs.getInt("id"));
                   a.setNombre(rs.getString("nombre"));
                   a.setEdad(rs.getInt("edad"));
                   a.setCarrera(rs.getString("carrera"));
                   a.setMaterias(rs.getInt("materias"));
                   
                   alumnos.add(a);
               }
               
            }
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }finally{
            db.disconnect();
        }
        
        return alumnos;
    }

    @Override
    public Alumno obtenerPorId(int id) {
        Alumno a = new Alumno();        
        try {
            StringBuilder sql = new StringBuilder("SELECT * FROM alumnos ");
            sql.append(" WHERE id =").append(id).append(" ");
            if(db.connect()){
               ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
               
               if(rs.next()){
                   a.setId(rs.getInt("id"));
                   a.setNombre(rs.getString("nombre"));
                   a.setEdad(rs.getInt("edad"));
                   a.setCarrera(rs.getString("carrera"));
                   a.setMaterias(rs.getInt("materias"));                   
               }
               
            }
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }finally{
            db.disconnect();
        }
        
        return a;
    }
    
}
