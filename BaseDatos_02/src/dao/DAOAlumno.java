package dao;

import entidades.Alumno;
import java.util.List;

public interface DAOAlumno {
    //CRUD
    //-CREATE -READ -UPDATE -DELETE
    public int agregar(Alumno alumno);
    public int editar(int id, Alumno alumno);
    public int eliminar(int id);
    
    public List<Alumno> obtenerTodos();
    public List<Alumno> obtenerPorNombre(String nombre);
    public Alumno obtenerPorId(int id);
}
