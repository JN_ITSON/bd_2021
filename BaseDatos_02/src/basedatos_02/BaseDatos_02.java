
package basedatos_02;

import configuracion.DBHelper;
import dao.DAOAlumno;
import dao.DAOAlumnoImpl;
import entidades.Alumno;
import java.util.List;

public class BaseDatos_02 {

   
    public static void main(String[] args) {
        DAOAlumno daoAlumno = new DAOAlumnoImpl();
        
        Alumno alumnoUno = new Alumno(0, "German Garmendia", 23, "Ingenieria en software", 8);
        int fila = daoAlumno.agregar(alumnoUno);
        if(fila > 0){
            alumnoUno.setId(fila);
            System.out.println(alumnoUno.toString());
        }else{
            System.out.println("No se pudo agregar al alumno");
        }
        
       /* List<Alumno> alumnos = daoAlumno.obtenerPorNombre("a");
        
        for(int i=0; i<alumnos.size(); i++){
            System.out.println(alumnos.get(i).toString());
        }
        Alumno maria = daoAlumno.obtenerPorId(5);
       // System.out.println(maria.toString());
        */
       
       
       for(Alumno al : daoAlumno.obtenerTodos()){
           System.out.println(al.toString());
       }
       
        
    }
    
}
