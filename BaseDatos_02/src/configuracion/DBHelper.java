/*
    Patrón de Diseño Singleton.
    Nos permite instanciar una clase solo una vez en la ejecución del proyecto.
    Las siguientes veces llama a la primer instancia creada.   

*/
package configuracion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author jorge
 */
public class DBHelper {
    
    private Connection connection;
    private Statement st;
    
    String url = "jdbc:mysql://localhost/bd_avanzada";
    String user = "root";
    String password = "";
    String error = ""; 
    
    private static DBHelper db = null;
    
    
    /* ===  INICIO DE SINGLETON ====*/
    public static DBHelper getDB(String url,String user, String password){
        if(db != null){
            db = new DBHelper(url,user,password);
            return db;
        }else{
            return db;
        }
    }
    
    
    private DBHelper(String url,String user, String password){
        this.url = url;
        this.user = user;
        this.password = password;
    }
    
    
    public static DBHelper getDB(){
        if(db == null){
            db = new DBHelper();
            return db;
        }else{
            return db;
        }
    }
     
    private DBHelper(){}
    /* ===  FIN DE SINGLETON ====*/
     
    /**
     Este metodo nos sirve para conectarnos a la base de datos.
     * Retorna verdadero si se pudo conectar.
     * caso contrario podemos obtener el error a traves de getError().
     */ 
    public boolean connect(){
        try {
            /*Esta linea crea una asociación entre nuestra aplicación
            y la clase Driver que está empaquetada en el jar de la libreria.
            */
            Class.forName("com.mysql.jdbc.Driver");
            /*
                La conexión se obtiene con DriverMaganager
                al pasar la url, user y password.
            */
            setConnection(DriverManager.getConnection(this.url,this.user,this.password));
        } catch (SQLException e) {
            this.error = e.getMessage();
            return false;
        } catch (ClassNotFoundException e) {
            this.error = e.getMessage();
            return false;
        }
        
        return true;
    }
    
    /**
     * Este metodo cierra la coneccion con mysql.
     */
    public void disconnect(){
        try {
            if(getConnection() != null){
                getConnection().close();
            }
        } catch (Exception e) {
            this.error = e.getMessage();
        }
    }
    
    /**
     * 
     * @param query String con la consulta de SQL
     * @param data_manipule Boolean TRUE(INSERT-UPDATE-DELETE) FALSE(SELECT)
     * @return Si data_manipule es verdadero return un tipo de dato entero
     * con el valor de la fila afectada,0 si no afectó nada, -1 si hubo error.
     * Si data_manipule es falso return un resultset ó null si hubo error.
     */
    public Object execute(String query,boolean data_manipule){
        
        try {
            st = getConnection().createStatement();
            
            if(data_manipule){//INSERT-UPDATE-DELETE
                int fila = 0;
                int resp = st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS );
                if(resp > 0){
                    ResultSet rs = st.getGeneratedKeys();
                    if(rs.next()){
                        fila = rs.getInt(1);
                    }
                }else{
                    fila = resp;
                }
                
                st.close();
                return fila;
            }else{//SELECT 
                ResultSet rs = st.executeQuery(query);
               // st.close();
                return rs;
            }
            
        } catch (Exception e) {
            this.error = e.getMessage();
            if(data_manipule){
                return -1;
            }else{
                return null;
            }
        }
        
        
    }
    
    
    
    //Getter And Setters

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
    
     
}
