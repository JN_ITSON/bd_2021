package entidades;

public class Alumno {
    //Atributos
    private int id = 0;
    private String nombre = new String();
    private int edad = 0;
    private String carrera = new String();
    private int materias = 0;
    
    //Constructores
    public Alumno(){
        
    }
    
    
    public Alumno(int id, String nombre, int edad, String carrera, int materias){
        this.id = id;
        this.nombre = nombre;
        this.edad = edad;
        this.carrera = carrera;
        this.materias = materias;
    }
    
    
    //Getters And Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public int getMaterias() {
        return materias;
    }

    public void setMaterias(int materias) {
        this.materias = materias;
    }
    
    //Metodos propios del objeto

    @Override
    public String toString() {
        String salida = "["+this.id+"]"+
                        "- "+this.nombre
                        +" [Carrera: "+this.carrera+"]";
        return salida;
    }
    
}
