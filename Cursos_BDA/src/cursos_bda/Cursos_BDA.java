package cursos_bda;

import configuracion.DBHelper;
import entidades.Alumno;
import entidades.Curso;
import interfaces.DAOCurso;
import interfaces.DAOCursoImpl;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class Cursos_BDA {

    public static void main(String[] args) {
   
        
        DAOCurso dao = new DAOCursoImpl();
        DBHelper db = DBHelper.getDB();
        int op = 0;
        String menu = "=====MENU=====";
               menu+= "\n1.-Agregar Curso";
               menu+= "\n2.-Agregar Alumno (a un curso)";
               menu+= "\n3.-Dar de baja Alumno";
               menu+= "\n4.-Mostrar cursos";
               menu+= "\n5.-Mostrar Lista de alumnos";
               menu+= "\n6.-Imprimir Lista de alumnos";
               menu+= "\n7.-Salir";
               
         do{
            //System.out.println(menu);
            op = Integer.valueOf(JOptionPane.showInputDialog(null,menu,"Digite una opción",JOptionPane.INFORMATION_MESSAGE));
            
            switch(op){
                case 1: 
                    
                    Curso c = new Curso();
                    c.setNombre(input("Digite el nombre del curso: "));
                    c.setDescripcion(input("Digite la descripcion del curso"));
                    c.setFechainicio(input("Digite la fecha de inicio del curso"));
                    c.setFechatermino(input("Digite la fecha de termino del curso"));
                    c.setHorario(input("Digite el horario del curso"));
                    
                    int id = dao.agregarCurso(c);
                    if(id > 0){
                      mensaje( "El curso se agregó correctamente.");
                    }else{                        
                      mensaje("No se pudo agregar el curso de forma correcta.");
                    }
                    break; //Agregar Curso
                case 2: 
                    Alumno a = new Alumno();
                    a.setNombre(input("Digite el nombre del alumno"));
                    a.setEmail(input("Digite el email"));
                    a.setEdad(Integer.valueOf(input("Digite la edad del alumno")));
                    a.setTelefono(input("Digite el telefono"));
                    
                    String cursosMsg = "===Seleccione un curso===";
                    for(Curso curso : dao.mostrarCursos()){
                        cursosMsg+= "\n"+curso.getId()+".- "+curso.getNombre();
                    }
                    a.setIdCurso(Integer.valueOf(input(cursosMsg)));
                    
                    int idA = dao.agregarAlumno(a);
                    if(idA > 0){
                      mensaje( "El alumno se agregó correctamente.");
                    }else{                        
                      mensaje("No se pudo agregar el alumno de forma correcta.");
                    }
                    
                    break; //Agregar Alumno
                case 3: 
                    int idCursoSel = 0;
                    int idAlumnoSel = 0;
                    String cursosMsgBaja = "===Seleccione un curso===";
                    for(Curso curso : dao.mostrarCursos()){
                        cursosMsgBaja+= "\n"+curso.getId()+".- "+curso.getNombre();
                    }
                    
                    idCursoSel = Integer.valueOf( input(cursosMsgBaja) );
                    
                    String listaAlumnoBaja = "======LISTADO======";
                    for(Alumno alumno : dao.mostrarListaAlumno(idCursoSel).getAlumnos()){
                        listaAlumnoBaja+= "\n"+alumno.getId()+".- "+alumno.getNombre();
                    }
                    
                    idAlumnoSel = Integer.valueOf(input(listaAlumnoBaja));
                    
                    dao.darBajaAlumno(idAlumnoSel);
                    mensaje("Se ha dado de baja al alumno");
                    
                    break; //Dar de baja alumno
                case 4:
                    
                    String cursosMsgMostrar = "===CURSOS===";
                    for(Curso curso : dao.mostrarCursos()){
                        cursosMsgMostrar+= "\n"+curso.getNombre()
                                           +" Fechas: "+curso.getFechainicio()+"-"+curso.getFechatermino()
                                           +"  Horario: "+curso.getHorario();
                    }
                    
                    mensaje(cursosMsgMostrar);
                    
                    break; //Mostrar cursos
                case 5:{
                    int idCurso = 0;
                    String cursosMsgMostrarAlumnos = "===Seleccione un curso===";
                    for(Curso curso : dao.mostrarCursos()){
                        cursosMsgMostrarAlumnos+= "\n"+curso.getId()+".- "+curso.getNombre();
                    }
                    
                    idCurso = Integer.valueOf( input(cursosMsgMostrarAlumnos) );
                    
                    String msgAlumnoLista = "===== Alumnos ===== ";
                    for(Alumno alumno: dao.mostrarListaAlumno(idCurso).getAlumnos()){
                        msgAlumnoLista += "\n"+alumno.getNombre()+" Email: "+alumno.getEmail();
                    }
                    
                    mensaje(msgAlumnoLista);
                    
                    break; //Mostrar lista de alumnos
                }
                case 6: {
                    
                    int idCurso = 0;
                    String cursosMsgMostrarAlumnos = "===Seleccione un curso===";
                    for(Curso curso : dao.mostrarCursos()){
                        cursosMsgMostrarAlumnos+= "\n"+curso.getId()+".- "+curso.getNombre();
                    }
                    
                    idCurso = Integer.valueOf( input(cursosMsgMostrarAlumnos) );
                    try {
                        if(db.connect()){
                            //Este el archivo de nuestro reporte
                            File reporte = new File("src/reportes/listaCurso.jasper");

                            //Definir los parametros
                            Map parametros = new HashMap();
                            parametros.put("idCurso", idCurso);

                            //Creamos el imprimible
                            JasperPrint print = JasperFillManager.fillReport(reporte.getPath(), parametros, db.getConnection());

                            JasperViewer viewer = new JasperViewer(print,false);
                            viewer.setVisible(true);
                            
                            while(viewer.isActive()){
                                
                            }
                            
                        }
                        
                    } catch (Exception e) {
                    }finally{
                        db.disconnect();
                    }
                    
                    
                    break; //Imprimir lista de alumnos
                }
                case 7: 
                    System.out.println("Gracias por utilizar nuestro sistema");
                    System.exit(0);
                    break; //Salir
                default:
                    System.out.println("Opción de menu no valida.");
                    break;
            }
             
         }while(true);      
        
        
    }
    
    private static String input(String msg){
        return JOptionPane.showInputDialog(msg);
    }
    
    private static void mensaje(String msg){
         JOptionPane.showMessageDialog(null,msg);
    }
    
}
