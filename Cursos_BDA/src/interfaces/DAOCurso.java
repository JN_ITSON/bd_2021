package interfaces;

import entidades.Alumno;
import entidades.Curso;
import java.util.List;

public interface DAOCurso {
    
    public int agregarCurso(Curso curso);
    public int agregarAlumno(Alumno alumno);
    public int darBajaAlumno(int idAlumno);
    public List<Curso> mostrarCursos();
    public Curso mostrarListaAlumno(int idCurso);
    
}
