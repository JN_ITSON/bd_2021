
package interfaces;

import configuracion.DBHelper;
import entidades.Alumno;
import entidades.Curso;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOCursoImpl implements DAOCurso{

    DBHelper db = DBHelper.getDB();
    
    @Override
    public int agregarCurso(Curso curso) {
        int result =0;
        
        try {
            
            StringBuilder sql = new StringBuilder();
            if(db.connect()){
                sql.append("INSERT INTO curso ")
                   .append(" (nombre,descripcion,fechaInicio,fechaFin,horario) ")
                   .append(" VALUES (")
                   .append("'").append(curso.getNombre()).append("',")
                   .append("'").append(curso.getDescripcion()).append("',")
                   .append("'").append(curso.getFechainicio()).append("',")
                   .append("'").append(curso.getFechatermino()).append("',")
                   .append("'").append(curso.getHorario()).append("'")
                   .append(")")    ;
                
                result = (int) db.execute(sql.toString(), true);
                
               /* if(result > 0){
                    for(Alumno alumno: curso.getAlumnos()){
                        alumno.setIdCurso(result);
                        this.agregarAlumno(alumno);
                    }
                }*/
                
            }else{
                System.out.println("Error DB: "+db.getError());
            }
            
        } catch (Exception e) {
            System.out.println("Error: "+e.getMessage());
            result = -1;
        }finally{
            db.disconnect();
        }
        
        return result;
    }

    @Override
    public int agregarAlumno(Alumno alumno) {
        int result =0;
        
        try {
            
            StringBuilder sql = new StringBuilder();
            if(db.connect()){
                sql.append("INSERT INTO alumno ")
                   .append(" (nombre,email,edad,telefono,idCurso) ")
                   .append(" VALUES (")
                   .append("'").append(alumno.getNombre()).append("',")
                   .append("'").append(alumno.getEmail()).append("',")
                   .append("").append(alumno.getEdad()).append(",")
                   .append("'").append(alumno.getTelefono()).append("',")
                   .append("").append(alumno.getIdCurso()).append("")
                   .append(")")    ;
                
                result = (int) db.execute(sql.toString(), true);                
                
            }else{
                System.out.println("Error DB: "+db.getError());
            }
            
        } catch (Exception e) {
            System.out.println("Error: "+e.getMessage());
            result = -1;
        }finally{
            db.disconnect();
        }
        
        return result;
    }

    @Override
    public int darBajaAlumno(int idAlumno) {
        int result =0;
        
        try {
            
            StringBuilder sql = new StringBuilder();
            if(db.connect()){
                sql.append("DELETE FROM alumno ")
                   .append(" WHERE id = ").append(idAlumno)
                       ;
                
                result = (int) db.execute(sql.toString(), true);                
                
            }else{
                System.out.println("Error DB: "+db.getError());
            }
            
        } catch (Exception e) {
            System.out.println("Error: "+e.getMessage());
            result = -1;
        }finally{
            db.disconnect();
        }
        
        return result;
    }

    @Override
    public List<Curso> mostrarCursos() {
        List<Curso> cursos = new ArrayList();
        
        try {
           StringBuilder sql = new StringBuilder(); 
           if(db.connect()){
               sql.append("SELECT * FROM curso ");
               
               ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
               
               while(rs.next()){
                   Curso curso = new Curso();
                   curso.setId(rs.getInt("id"));
                   curso.setNombre(rs.getString("nombre"));
                   curso.setDescripcion(rs.getString("descripcion"));
                   curso.setFechainicio(rs.getString("fechaInicio"));
                   curso.setFechatermino(rs.getString("fechaFin"));
                   curso.setHorario(rs.getString("horario"));
                   
                   cursos.add(curso);
               }
               
           }else{
               System.out.println("Error DB: "+db.getError());
           }
        } catch (Exception e) {
            System.out.println("Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }
        
        return cursos;
    }

    @Override
    public Curso mostrarListaAlumno(int idCurso) {
      
        Curso curso = new Curso();
        
        try {
           StringBuilder sql = new StringBuilder(); 
           if(db.connect()){
               sql.append("SELECT * FROM curso WHERE id =  ").append(idCurso);
               
               ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
               
               while(rs.next()){
                   
                   curso.setId(rs.getInt("id"));
                   curso.setNombre(rs.getString("nombre"));
                   curso.setDescripcion(rs.getString("descripcion"));
                   curso.setFechainicio(rs.getString("fechaInicio"));
                   curso.setFechatermino(rs.getString("fechaFin"));
                   curso.setHorario(rs.getString("horario"));
                   
                   
               }
               
               sql = new StringBuilder();
               sql.append(" SELECT * FROM alumno WHERE idCurso = ").append(idCurso);
               
               ResultSet rsAl = (ResultSet) db.execute(sql.toString(), false);
               
               while(rsAl.next()){
                   Alumno alumno = new Alumno();
                   alumno.setId(rsAl.getInt("id"));
                   alumno.setNombre(rsAl.getString("nombre"));
                   alumno.setEdad(rsAl.getInt("edad"));
                   alumno.setEmail(rsAl.getString("email"));
                   alumno.setTelefono(rsAl.getString("telefono"));
                   alumno.setIdCurso(idCurso);
                   
                   curso.getAlumnos().add(alumno);
                   
               }
               
           }else{
               System.out.println("Error DB: "+db.getError());
           }
        } catch (Exception e) {
            System.out.println("Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }
        
        return curso;
    }
    
}
