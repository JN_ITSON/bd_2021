
package controlador;

import configuracion.DBHelper;
import entidades.Smartphone;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class SmartphoneDAOImpl implements iCRUD<Smartphone> {

    DBHelper db = DBHelper.getDB();
    
    @Override
    public int agregar(Smartphone smart) {
        int res = -1;
        
        try {
            if(db.connect()){
                StringBuilder sql = new StringBuilder();
                //Trabajar la consulta
                sql.append("INSERT INTO smartphone ")
                        .append(" (nombre, descripcion, procesador, ram, pantalla, almacenamiento, camara, conectividad, precio, stock)")
                        .append(" VALUES ( ")
                            .append("'").append(smart.getNombre()).append("',")
                            .append("'").append(smart.getDescripcion()).append("',")
                            .append("'").append(smart.getProcesador()).append("',")
                            .append("'").append(smart.getRam()).append("',")
                            .append("'").append(smart.getPantalla()).append("',")
                            .append("'").append(smart.getAlmacenamiento()).append("', ")
                            .append("'").append(smart.getCamara()).append("', ")
                            .append("'").append(smart.getConectividad()).append("', ")
                            .append(" ").append(smart.getPrecio()).append(", ")
                            .append(" ").append(smart.getStock()).append(" ")
                        .append(")");
                
                res = (int) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }        
        return res;
    }

    @Override
    public int editar(Smartphone smart) {
        int res = -1;
        
        try {
            if(db.connect()){
                StringBuilder sql = new StringBuilder();
                //Trabajar la consulta
                sql.append("UPDATE smartphone SET ")
                        .append(" nombre = '").append(smart.getNombre()).append("',")
                        .append(" descripcion = '").append(smart.getDescripcion()).append("',")
                        .append(" procesador = '").append(smart.getProcesador()).append("',")
                        .append(" ram = '").append(smart.getRam()).append("',")
                        .append(" pantalla = '").append(smart.getPantalla()).append("',")
                        .append(" almacenamiento = '").append(smart.getAlmacenamiento()).append("', ")
                        .append(" camara = '").append(smart.getCamara()).append("', ")
                        .append(" conectividad = '").append(smart.getConectividad()).append("', ")
                        .append(" precio =  ").append(smart.getPrecio()).append(", ")
                        .append(" stock =  ").append(smart.getStock()).append(" ")
                    .append(" WHERE id = ").append(smart.getId());
                
                res = (int) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }        
        return res;
    }

    @Override
    public int eliminar(int id) {
        int res = -1;
        
        try {
            if(db.connect()){
                StringBuilder sql = new StringBuilder();
                //Trabajar la consulta
                sql.append("DELETE FROM smartphone WHERE id = ").append(id);
                
                res = (int) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }        
        return res;
    }

    @Override
    public List<Smartphone> buscarTodos() {
        List<Smartphone> smarts = new ArrayList();
        
        try {
            if(db.connect()){
                //Trabajar con bd
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT * FROM smartphone ");
                ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
                
                while(rs.next()){
                    Smartphone smart = new Smartphone();
                    
                    smart.setId(rs.getInt("id"));
                    smart.setNombre(rs.getString("nombre"));
                    smart.setDescripcion(rs.getString("descripcion"));
                    smart.setProcesador(rs.getString("procesador"));
                    smart.setRam(rs.getString("ram"));
                    smart.setPantalla(rs.getString("pantalla"));
                    smart.setAlmacenamiento(rs.getString("almacenamiento"));
                    smart.setCamara(rs.getString("camara"));
                    smart.setConectividad(rs.getString("conectividad"));
                    smart.setPrecio(rs.getDouble("precio"));
                    smart.setStock(rs.getInt("stock"));
                    
                    
                    smarts.add(smart);
                    
                }
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }
        
        
        return smarts;
    }

    @Override
    public List<Smartphone> buscarFiltro(String columna, String valor) {
        List<Smartphone> smarts = new ArrayList();
        
        try {
            if(db.connect()){
                //Trabajar con bd
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT * FROM smartphone ")
                    .append(" WHERE ").append(columna)
                        .append(" LIKE '%").append(valor).append("%'");
                ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
                
                while(rs.next()){
                    Smartphone smart = new Smartphone();
                    
                    smart.setId(rs.getInt("id"));
                    smart.setNombre(rs.getString("nombre"));
                    smart.setDescripcion(rs.getString("descripcion"));
                    smart.setProcesador(rs.getString("procesador"));
                    smart.setRam(rs.getString("ram"));
                    smart.setPantalla(rs.getString("pantalla"));
                    smart.setAlmacenamiento(rs.getString("almacenamiento"));
                    smart.setCamara(rs.getString("camara"));
                    smart.setConectividad(rs.getString("conectividad"));
                    smart.setPrecio(rs.getDouble("precio"));
                    smart.setStock(rs.getInt("stock"));
                    
                    
                    smarts.add(smart);
                    
                }
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }
        
        
        return smarts;
    }

    @Override
    public Smartphone buscarId(int id) {
        
        Smartphone smart = new Smartphone();
        
        try {
            if(db.connect()){
                //Trabajar con bd
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT * FROM smartphone ")
                    .append(" WHERE id = ").append(id);
                ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
                
                while(rs.next()){
                    
                    smart.setId(rs.getInt("id"));
                    smart.setNombre(rs.getString("nombre"));
                    smart.setDescripcion(rs.getString("descripcion"));
                    smart.setProcesador(rs.getString("procesador"));
                    smart.setRam(rs.getString("ram"));
                    smart.setPantalla(rs.getString("pantalla"));
                    smart.setAlmacenamiento(rs.getString("almacenamiento"));
                    smart.setCamara(rs.getString("camara"));
                    smart.setConectividad(rs.getString("conectividad"));
                    smart.setPrecio(rs.getDouble("precio"));
                    smart.setStock(rs.getInt("stock"));
                                        
                }
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }
        
        return smart;
    }
    
   
    
   public int modificarStock(int id, int totalStock){ 
         int res = -1;
        
        try {
            if(db.connect()){
                StringBuilder sql = new StringBuilder();
                //Trabajar la consulta
                sql.append("UPDATE smartphone SET ")                        
                        .append(" stock =  ").append(totalStock).append(" ")
                    .append(" WHERE id = ").append(id);
                
                res = (int) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }        
        return res;
    }
     
    
}
