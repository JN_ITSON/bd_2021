package controlador;

import configuracion.DBHelper;
import entidades.Persona;
import entidades.Smartphone;
import entidades.Venta;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class VentaDAOImpl implements iCRUD<Venta> {

    DBHelper db = DBHelper.getDB();
    
    @Override
    public int agregar(Venta venta) {
        int res = -1;
        
        try {
            if(db.connect()){
                StringBuilder sql = new StringBuilder();
                //Trabajar la consulta
                sql.append("INSERT INTO venta ")
                        .append(" (cliente, vendedor, fecha, total)")
                        .append(" VALUES ( ")
                            .append(" ").append(venta.getCliente().getId()).append(", ")
                            .append(" ").append(venta.getVendedor().getId()).append(", ")
                            .append("'").append(venta.getFecha()).append("',")
                            .append(" ").append(venta.getTotal()).append(" ")
                        .append(")");
               
                res = (int) db.execute(sql.toString(), true);
                
                if(res > 0){ // Id venta
                    
                    for(int i=0; i<venta.getSmartphones().size(); i++){
                        sql = new StringBuilder();
                        sql.append("INSERT INTO venta_smartphones (venta,smarthpone) ")
                           .append(" VALUES (")
                            .append(res).append(", ")
                            .append(venta.getSmartphones().get(i).getId())                                     
                           .append(")");
                        db.execute(sql.toString(), true);
                    }
                    
                }
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }        
        return res;
    }

    @Override
    public int editar(Venta venta) {
        int res = -1;
        
        try {
            if(db.connect()){
                StringBuilder sql = new StringBuilder();
                //Trabajar la consulta
                sql.append("UPDATE venta SET")
                            .append(" cliente = ").append(venta.getCliente().getId()).append(", ")
                            .append(" vendedor = ").append(venta.getVendedor().getId()).append(", ")
                            .append(" fecha = '").append(venta.getFecha()).append("',")
                            .append(" total = ").append(venta.getTotal()).append(" ")
                        .append(" WHERE id = ").append(venta.getId());
                
                res = (int) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }        
        return res;
    }

    @Override
    public int eliminar(int id) {
        
        int res = -1;
        
        try {
            if(db.connect()){
                StringBuilder sql = new StringBuilder();
                //Trabajar la consulta
                sql.append("DELETE FROM venta_smartphones WHERE venta = ").append(id);
                db.execute(sql.toString(), true);
                
                sql = new StringBuilder();
                sql.append("DELETE FROM venta WHERE id = ").append(id);                
                res = (int) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }        
        return res;
    }

    @Override
    public List<Venta> buscarTodos() {
        List<Venta> ventas = new ArrayList();
        
        try {
            if(db.connect()){
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT  ")
                            .append("	venta.id AS 'idVenta',  ")
                            .append("    venta.fecha AS 'fechaVenta',  ")
                            .append("    venta.total AS 'totalVenta',  ")
                            .append("    cliente.id AS 'idCliente',  ")
                            .append("    cliente.nombre AS 'nombreCliente',  ")
                            .append("    cliente.domicilio AS 'domicilioCliente',  ")
                            .append("    cliente.telefono AS 'telefonoCliente',  ")
                            .append("    cliente.email AS 'emailCliente',  ")
                            .append("    cliente.rfc AS 'rfcCliente',  ")
                            .append("    cliente.domicilioFiscal AS 'domFCliente',  ")
                            .append("    vendedor.id AS 'idVendedor',  ")
                            .append("    vendedor.nombre AS 'nombreVendedor',  ")
                            .append("    vendedor.domicilio AS 'domicilioVendedor',  ")
                            .append("    vendedor.telefono AS 'telefonoVendedor',  ")
                            .append("    vendedor.email AS 'emailVendedor',  ")
                            .append("    vendedor.rfc AS 'rfcVendedor',  ")
                            .append("    vendedor.domicilioFiscal AS 'domFVendedor',  ")
                            .append("    smart.id AS 'idSmart',  ")
                            .append("    smart.nombre AS 'nombreSmart',  ")
                            .append("    smart.descripcion,  ")
                            .append("    smart.procesador,  ")
                            .append("    smart.ram,  ")
                            .append("    smart.pantalla,  ")
                            .append("    smart.almacenamiento,  ")
                            .append("    smart.camara,  ")
                            .append("    smart.conectividad,  ")
                            .append("    smart.precio,  ")
                            .append("    smart.stock  ")
                            .append(" FROM venta  ")
                            .append(" LEFT JOIN persona cliente ON cliente.id = venta.cliente  ")
                            .append(" LEFT JOIN persona vendedor ON vendedor.id = venta.vendedor  ")
                            .append(" LEFT JOIN venta_smartphones vs ON vs.venta = venta.id  ")
                            .append(" LEFT JOIN smartphone smart ON smart.id = vs.smartphone ");
                
                ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
                
                Venta v = new Venta();
                int bandera = 0; //Aqui voy el id de la venta
                while(rs.next()){
                    
                    if(rs.getInt("idVenta") != bandera){ //Inicializar de nuevo venta
                        
                        if(v.getId() > 0){ //Aqui si la venta tiene datos la agregamos a la lista
                            ventas.add(v); //antes de resetear
                        }
                        
                        v = new Venta();
                        bandera = rs.getInt("idVenta");
                        
                        //Llenamos los datos que se duplican
                        v.setId(rs.getInt("idVenta"));
                        v.setFecha(rs.getString("fechaVenta"));
                        v.setTotal(rs.getDouble("totalVenta"));
                        
                        //Tambien llenamos al cliente y al vendedor
                        Persona cliente = new Persona();
                            cliente.setId(rs.getInt("idCliente"));
                            cliente.setNombre(rs.getString("nombreCliente"));
                            cliente.setDomicilio(rs.getString("domicilioCliente"));
                            cliente.setEmail(rs.getString("emailCliente"));
                            cliente.setDomicilioFiscal(rs.getString("domFCliente"));
                            cliente.setRFC(rs.getString("rfcCliente"));
                            cliente.setTelefono(rs.getString("telefonoCliente"));
                        v.setCliente(cliente);
                        
                        
                        Persona vendedor = new Persona();
                           vendedor.setId(rs.getInt("idVendedor"));
                           vendedor.setNombre(rs.getString("nombreVendedor"));
                           vendedor.setDomicilio(rs.getString("domicilioVendedor"));
                           vendedor.setEmail(rs.getString("emailVendedor"));
                           vendedor.setDomicilioFiscal(rs.getString("domFVendedor"));
                           vendedor.setRFC(rs.getString("rfcVendedor"));
                           vendedor.setTelefono(rs.getString("telefonoVendedor"));
                        v.setVendedor(vendedor);
                        
                    }
                    
                    Smartphone smart = new Smartphone();
                    smart.setId(rs.getInt("idSmart"));
                    smart.setNombre(rs.getString("nombreSmart"));
                    smart.setDescripcion(rs.getString("descripcion"));
                    smart.setProcesador(rs.getString("procesador"));
                    smart.setRam(rs.getString("ram"));
                    smart.setPantalla(rs.getString("pantalla"));
                    smart.setAlmacenamiento(rs.getString("almacenamiento"));
                    smart.setCamara(rs.getString("camara"));
                    smart.setConectividad(rs.getString("conectividad"));
                    smart.setPrecio(rs.getDouble("precio"));
                    smart.setStock(rs.getInt("stock"));
                    
                    v.getSmartphones().add(smart);
                    
                }
                
                ventas.add(v);//Es para agregar la ultima venta
                
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }
        
        return ventas;
    }

    @Override
    public List<Venta> buscarFiltro(String columna, String valor) {
         List<Venta> ventas = new ArrayList();
        
        try {
            if(db.connect()){
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT  ")
                            .append("	venta.id AS 'idVenta',  ")
                            .append("    venta.fecha AS 'fechaVenta',  ")
                            .append("    venta.total AS 'totalVenta',  ")
                            .append("    cliente.id AS 'idCliente',  ")
                            .append("    cliente.nombre AS 'nombreCliente',  ")
                            .append("    cliente.domicilio AS 'domicilioCliente',  ")
                            .append("    cliente.telefono AS 'telefonoCliente',  ")
                            .append("    cliente.email AS 'emailCliente',  ")
                            .append("    cliente.rfc AS 'rfcCliente',  ")
                            .append("    cliente.domicilioFiscal AS 'domFCliente',  ")
                            .append("    vendedor.id AS 'idVendedor',  ")
                            .append("    vendedor.nombre AS 'nombreVendedor',  ")
                            .append("    vendedor.domicilio AS 'domicilioVendedor',  ")
                            .append("    vendedor.telefono AS 'telefonoVendedor',  ")
                            .append("    vendedor.email AS 'emailVendedor',  ")
                            .append("    vendedor.rfc AS 'rfcVendedor',  ")
                            .append("    vendedor.domicilioFiscal AS 'domFVendedor',  ")
                            .append("    smart.id AS 'idSmart',  ")
                            .append("    smart.nombre AS 'nombreSmart',  ")
                            .append("    smart.descripcion,  ")
                            .append("    smart.procesador,  ")
                            .append("    smart.ram,  ")
                            .append("    smart.pantalla,  ")
                            .append("    smart.almacenamiento,  ")
                            .append("    smart.camara,  ")
                            .append("    smart.conectividad,  ")
                            .append("    smart.precio,  ")
                            .append("    smart.stock  ")
                            .append(" FROM venta  ")
                            .append(" LEFT JOIN persona cliente ON cliente.id = venta.cliente  ")
                            .append(" LEFT JOIN persona vendedor ON vendedor.id = venta.vendedor  ")
                            .append(" LEFT JOIN venta_smartphones vs ON vs.venta = venta.id  ")
                            .append(" LEFT JOIN smartphone smart ON smart.id = vs.smartphone ")
                            .append(" WHERE ").append(columna)
                            .append(" LIKE '%").append(valor).append("%'");
                
                ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
                
                Venta v = new Venta();
                int bandera = 0; //Aqui voy el id de la venta
                while(rs.next()){
                    
                    if(rs.getInt("idVenta") != bandera){ //Inicializar de nuevo venta
                        
                        if(v.getId() > 0){ //Aqui si la venta tiene datos la agregamos a la lista
                            ventas.add(v); //antes de resetear
                        }
                        
                        v = new Venta();
                        bandera = rs.getInt("idVenta");
                        
                        //Llenamos los datos que se duplican
                        v.setId(rs.getInt("idVenta"));
                        v.setFecha(rs.getString("fechaVenta"));
                        v.setTotal(rs.getDouble("totalVenta"));
                        
                        //Tambien llenamos al cliente y al vendedor
                        Persona cliente = new Persona();
                            cliente.setId(rs.getInt("idCliente"));
                            cliente.setNombre(rs.getString("nombreCliente"));
                            cliente.setDomicilio(rs.getString("domicilioCliente"));
                            cliente.setEmail(rs.getString("emailCliente"));
                            cliente.setDomicilioFiscal(rs.getString("domFCliente"));
                            cliente.setRFC(rs.getString("rfcCliente"));
                            cliente.setTelefono(rs.getString("telefonoCliente"));
                        v.setCliente(cliente);
                        
                        
                        Persona vendedor = new Persona();
                           vendedor.setId(rs.getInt("idVendedor"));
                           vendedor.setNombre(rs.getString("nombreVendedor"));
                           vendedor.setDomicilio(rs.getString("domicilioVendedor"));
                           vendedor.setEmail(rs.getString("emailVendedor"));
                           vendedor.setDomicilioFiscal(rs.getString("domFVendedor"));
                           vendedor.setRFC(rs.getString("rfcVendedor"));
                           vendedor.setTelefono(rs.getString("telefonoVendedor"));
                        v.setVendedor(vendedor);
                        
                    }
                    
                    Smartphone smart = new Smartphone();
                    smart.setId(rs.getInt("idSmart"));
                    smart.setNombre(rs.getString("nombreSmart"));
                    smart.setDescripcion(rs.getString("descripcion"));
                    smart.setProcesador(rs.getString("procesador"));
                    smart.setRam(rs.getString("ram"));
                    smart.setPantalla(rs.getString("pantalla"));
                    smart.setAlmacenamiento(rs.getString("almacenamiento"));
                    smart.setCamara(rs.getString("camara"));
                    smart.setConectividad(rs.getString("conectividad"));
                    smart.setPrecio(rs.getDouble("precio"));
                    smart.setStock(rs.getInt("stock"));
                    
                    v.getSmartphones().add(smart);
                    
                }
                
                ventas.add(v);//Es para agregar la ultima venta
                
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }
        
        return ventas;
    }

    @Override
    public Venta buscarId(int id) {
       Venta v = new Venta();
       
        try {
            if(db.connect()){
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT  ")
                            .append("	venta.id AS 'idVenta',  ")
                            .append("    venta.fecha AS 'fechaVenta',  ")
                            .append("    venta.total AS 'totalVenta',  ")
                            .append("    cliente.id AS 'idCliente',  ")
                            .append("    cliente.nombre AS 'nombreCliente',  ")
                            .append("    cliente.domicilio AS 'domicilioCliente',  ")
                            .append("    cliente.telefono AS 'telefonoCliente',  ")
                            .append("    cliente.email AS 'emailCliente',  ")
                            .append("    cliente.rfc AS 'rfcCliente',  ")
                            .append("    cliente.domicilioFiscal AS 'domFCliente',  ")
                            .append("    vendedor.id AS 'idVendedor',  ")
                            .append("    vendedor.nombre AS 'nombreVendedor',  ")
                            .append("    vendedor.domicilio AS 'domicilioVendedor',  ")
                            .append("    vendedor.telefono AS 'telefonoVendedor',  ")
                            .append("    vendedor.email AS 'emailVendedor',  ")
                            .append("    vendedor.rfc AS 'rfcVendedor',  ")
                            .append("    vendedor.domicilioFiscal AS 'domFVendedor',  ")
                            .append("    smart.id AS 'idSmart',  ")
                            .append("    smart.nombre AS 'nombreSmart',  ")
                            .append("    smart.descripcion,  ")
                            .append("    smart.procesador,  ")
                            .append("    smart.ram,  ")
                            .append("    smart.pantalla,  ")
                            .append("    smart.almacenamiento,  ")
                            .append("    smart.camara,  ")
                            .append("    smart.conectividad,  ")
                            .append("    smart.precio,  ")
                            .append("    smart.stock  ")
                            .append(" FROM venta  ")
                            .append(" LEFT JOIN persona cliente ON cliente.id = venta.cliente  ")
                            .append(" LEFT JOIN persona vendedor ON vendedor.id = venta.vendedor  ")
                            .append(" LEFT JOIN venta_smartphones vs ON vs.venta = venta.id  ")
                            .append(" LEFT JOIN smartphone smart ON smart.id = vs.smartphone ")
                            .append(" WHERE venta.id = ").append(id);
                
                ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
                
                
                int bandera = 0; //Aqui voy el id de la venta
                while(rs.next()){
                    
                    if(rs.getInt("idVenta") != bandera){ //Inicializar de nuevo venta
                        
                        v = new Venta();
                        bandera = rs.getInt("idVenta");
                        
                        //Llenamos los datos que se duplican
                        v.setId(rs.getInt("idVenta"));
                        v.setFecha(rs.getString("fechaVenta"));
                        v.setTotal(rs.getDouble("totalVenta"));
                        
                        //Tambien llenamos al cliente y al vendedor
                        Persona cliente = new Persona();
                            cliente.setId(rs.getInt("idCliente"));
                            cliente.setNombre(rs.getString("nombreCliente"));
                            cliente.setDomicilio(rs.getString("domicilioCliente"));
                            cliente.setEmail(rs.getString("emailCliente"));
                            cliente.setDomicilioFiscal(rs.getString("domFCliente"));
                            cliente.setRFC(rs.getString("rfcCliente"));
                            cliente.setTelefono(rs.getString("telefonoCliente"));
                        v.setCliente(cliente);
                        
                        
                        Persona vendedor = new Persona();
                           vendedor.setId(rs.getInt("idVendedor"));
                           vendedor.setNombre(rs.getString("nombreVendedor"));
                           vendedor.setDomicilio(rs.getString("domicilioVendedor"));
                           vendedor.setEmail(rs.getString("emailVendedor"));
                           vendedor.setDomicilioFiscal(rs.getString("domFVendedor"));
                           vendedor.setRFC(rs.getString("rfcVendedor"));
                           vendedor.setTelefono(rs.getString("telefonoVendedor"));
                        v.setVendedor(vendedor);
                        
                    }
                    
                    Smartphone smart = new Smartphone();
                    smart.setId(rs.getInt("idSmart"));
                    smart.setNombre(rs.getString("nombreSmart"));
                    smart.setDescripcion(rs.getString("descripcion"));
                    smart.setProcesador(rs.getString("procesador"));
                    smart.setRam(rs.getString("ram"));
                    smart.setPantalla(rs.getString("pantalla"));
                    smart.setAlmacenamiento(rs.getString("almacenamiento"));
                    smart.setCamara(rs.getString("camara"));
                    smart.setConectividad(rs.getString("conectividad"));
                    smart.setPrecio(rs.getDouble("precio"));
                    smart.setStock(rs.getInt("stock"));
                    
                    v.getSmartphones().add(smart);
                    
                }
                                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }
        
        return v;
    }
}
