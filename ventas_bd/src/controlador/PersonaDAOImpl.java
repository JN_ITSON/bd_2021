package controlador;

import configuracion.DBHelper;
import entidades.Persona;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class PersonaDAOImpl implements iCRUD<Persona> {

    DBHelper db = DBHelper.getDB();
    
    @Override
    public int agregar(Persona persona) {
        int res = -1;
        
        try {
            if(db.connect()){
                StringBuilder sql = new StringBuilder();
                //Trabajar la consulta
                sql.append("INSERT INTO persona ")
                        .append(" (nombre,domicilio,telefono,email,rfc,domicilioFiscal)")
                        .append(" VALUES ( ")
                            .append("'").append(persona.getNombre()).append("',")
                            .append("'").append(persona.getDomicilio()).append("',")
                            .append("'").append(persona.getTelefono()).append("',")
                            .append("'").append(persona.getEmail()).append("',")
                            .append("'").append(persona.getRfc()).append("',")
                            .append("'").append(persona.getDomicilioFiscal()).append("' ")
                        .append(")");
                
                res = (int) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }        
        return res;
    }

    @Override
    public int editar(Persona persona) {
        int res = -1;
        
        try {
            if(db.connect()){
                StringBuilder sql = new StringBuilder();
                //Trabajar la consulta
                sql.append("UPDATE persona SET ")
                            .append("nombre = '").append(persona.getNombre()).append("',")
                            .append("domicilio = '").append(persona.getDomicilio()).append("',")
                            .append("telefono = '").append(persona.getTelefono()).append("',")
                            .append("email = '").append(persona.getEmail()).append("',")
                            .append("rfc = '").append(persona.getRfc()).append("',")
                            .append("domicilioFiscal = '").append(persona.getDomicilioFiscal()).append("' ")
                        .append(" WHERE id = ").append(persona.getId());
                
                res = (int) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }        
        return res;
    }

    @Override
    public int eliminar(int id) {
        int res = -1;
        
        try {
            if(db.connect()){
                StringBuilder sql = new StringBuilder();
                //Trabajar la consulta
                sql.append("DELETE FROM persona WHERE id = ").append(id);
                
                res = (int) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }        
        return res;
    }

    @Override
    public List<Persona> buscarTodos() {
        List<Persona> personas = new ArrayList();
        
        try {
            if(db.connect()){
                //Trabajar con bd
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT * FROM persona ");
                ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
                
                while(rs.next()){
                    Persona p = new Persona();
                    
                    p.setId(rs.getInt("id"));
                    p.setNombre(rs.getString("nombre"));
                    p.setDomicilio(rs.getString("domicilio"));
                    p.setEmail(rs.getString("email"));
                    p.setDomicilioFiscal(rs.getString("domicilioFiscal"));
                    p.setRFC(rs.getString("rfc"));
                    p.setTelefono(rs.getString("telefono"));
                    
                    personas.add(p);
                    
                }
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }
        
        
        return personas;
    }

    @Override
    public List<Persona> buscarFiltro(String columna, String valor) {
        List<Persona> personas = new ArrayList();
        
        try {
            if(db.connect()){
                //Trabajar con bd
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT * FROM persona  ")
                    .append(" WHERE ").append(columna)
                        .append(" LIKE '%").append(valor).append("%'");
                
                ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
                
                while(rs.next()){
                    Persona p = new Persona();
                    
                    p.setId(rs.getInt("id"));
                    p.setNombre(rs.getString("nombre"));
                    p.setDomicilio(rs.getString("domicilio"));
                    p.setEmail(rs.getString("email"));
                    p.setDomicilioFiscal(rs.getString("domicilioFiscal"));
                    p.setRFC(rs.getString("rfc"));
                    p.setTelefono(rs.getString("telefono"));
                    
                    personas.add(p);
                    
                }
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }
        
        
        return personas;
    }

    @Override
    public Persona buscarId(int id) {
        Persona p = new Persona();
        
        try {
            if(db.connect()){
                //Trabajar con bd
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT * FROM persona  ")
                    .append(" WHERE id = ").append(id)
                        ;
                
                ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
                
                if(rs.next()){
                    
                    p.setId(rs.getInt("id"));
                    p.setNombre(rs.getString("nombre"));
                    p.setDomicilio(rs.getString("domicilio"));
                    p.setEmail(rs.getString("email"));
                    p.setDomicilioFiscal(rs.getString("domicilioFiscal"));
                    p.setRFC(rs.getString("rfc"));
                    p.setTelefono(rs.getString("telefono"));                   
                    
                }
                
            }else{
                System.out.println("Error: "+db.getError());
            }
        } catch (Exception e) {
            System.out.println("System Error: "+e.getMessage());
        }finally{
            db.disconnect();
        }
        return p;
    }

  
    
}
