package controlador;

import java.util.List;


public interface iCRUD<T> {
    
    public int agregar(T t);
    public int editar(T t);
    public int eliminar(int id);
    
    public List<T> buscarTodos();
    public List<T> buscarFiltro(String columna,String valor);
    public T buscarId(int id);
    
}
