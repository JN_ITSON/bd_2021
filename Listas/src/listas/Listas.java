/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jorge
 */
public class Listas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        List<String> nombres = new ArrayList();
        
        nombres.add("Xiaomi");
        nombres.add("iphone");
        nombres.add("Samsung");
        nombres.add("pocophone");
        nombres.add("pocophone2");
        
        nombres.set(3, "Hawei"); //Reemplaza el elemento, si no hay retorna un error
        nombres.remove(3);
        
        for(int i =0; i<Byte.MAX_VALUE; i++){
            nombres.add("Telefono "+(i+1));
        }
//        
//        for(int i=0; i<nombres.size(); i++){
//            System.out.println(nombres.get(i));
//        }
        
        for(String nombre  : nombres){
            System.out.println(nombre);
        }
        
        
    }
    
}
